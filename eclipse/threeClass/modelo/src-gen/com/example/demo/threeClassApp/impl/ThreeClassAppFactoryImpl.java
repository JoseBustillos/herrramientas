/**
 */
package com.example.demo.threeClassApp.impl;

import com.example.demo.threeClassApp.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ThreeClassAppFactoryImpl extends EFactoryImpl implements ThreeClassAppFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ThreeClassAppFactory init() {
		try {
			ThreeClassAppFactory theThreeClassAppFactory = (ThreeClassAppFactory) EPackage.Registry.INSTANCE
					.getEFactory(ThreeClassAppPackage.eNS_URI);
			if (theThreeClassAppFactory != null) {
				return theThreeClassAppFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ThreeClassAppFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThreeClassAppFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case ThreeClassAppPackage.PRESCRIPCION:
			return createPrescripcion();
		case ThreeClassAppPackage.MEDICAMENTO:
			return createMedicamento();
		case ThreeClassAppPackage.TRATAMIENTO:
			return createTratamiento();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Prescripcion createPrescripcion() {
		PrescripcionImpl prescripcion = new PrescripcionImpl();
		return prescripcion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Medicamento createMedicamento() {
		MedicamentoImpl medicamento = new MedicamentoImpl();
		return medicamento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tratamiento createTratamiento() {
		TratamientoImpl tratamiento = new TratamientoImpl();
		return tratamiento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThreeClassAppPackage getThreeClassAppPackage() {
		return (ThreeClassAppPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ThreeClassAppPackage getPackage() {
		return ThreeClassAppPackage.eINSTANCE;
	}

} //ThreeClassAppFactoryImpl
