/**
 */
package com.example.demo.threeClassApp.impl;

import com.example.demo.threeClassApp.Medicamento;
import com.example.demo.threeClassApp.ThreeClassAppPackage;
import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Medicamento</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.example.demo.threeClassApp.impl.MedicamentoImpl#getIdMedicamento <em>Id Medicamento</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.impl.MedicamentoImpl#getCostoMedicamento <em>Costo Medicamento</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.impl.MedicamentoImpl#getDetalleMedicamento <em>Detalle Medicamento</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MedicamentoImpl extends MinimalEObjectImpl.Container implements Medicamento {
	/**
	 * The default value of the '{@link #getIdMedicamento() <em>Id Medicamento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdMedicamento()
	 * @generated
	 * @ordered
	 */
	protected static final long ID_MEDICAMENTO_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getIdMedicamento() <em>Id Medicamento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdMedicamento()
	 * @generated
	 * @ordered
	 */
	protected long idMedicamento = ID_MEDICAMENTO_EDEFAULT;

	/**
	 * The default value of the '{@link #getCostoMedicamento() <em>Costo Medicamento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCostoMedicamento()
	 * @generated
	 * @ordered
	 */
	protected static final String COSTO_MEDICAMENTO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCostoMedicamento() <em>Costo Medicamento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCostoMedicamento()
	 * @generated
	 * @ordered
	 */
	protected String costoMedicamento = COSTO_MEDICAMENTO_EDEFAULT;

	/**
	 * The default value of the '{@link #getDetalleMedicamento() <em>Detalle Medicamento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetalleMedicamento()
	 * @generated
	 * @ordered
	 */
	protected static final String DETALLE_MEDICAMENTO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDetalleMedicamento() <em>Detalle Medicamento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetalleMedicamento()
	 * @generated
	 * @ordered
	 */
	protected String detalleMedicamento = DETALLE_MEDICAMENTO_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MedicamentoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ThreeClassAppPackage.Literals.MEDICAMENTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getIdMedicamento() {
		return idMedicamento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdMedicamento(long newIdMedicamento) {
		long oldIdMedicamento = idMedicamento;
		idMedicamento = newIdMedicamento;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreeClassAppPackage.MEDICAMENTO__ID_MEDICAMENTO,
					oldIdMedicamento, idMedicamento));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCostoMedicamento() {
		return costoMedicamento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCostoMedicamento(String newCostoMedicamento) {
		String oldCostoMedicamento = costoMedicamento;
		costoMedicamento = newCostoMedicamento;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreeClassAppPackage.MEDICAMENTO__COSTO_MEDICAMENTO,
					oldCostoMedicamento, costoMedicamento));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDetalleMedicamento() {
		return detalleMedicamento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDetalleMedicamento(String newDetalleMedicamento) {
		String oldDetalleMedicamento = detalleMedicamento;
		detalleMedicamento = newDetalleMedicamento;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreeClassAppPackage.MEDICAMENTO__DETALLE_MEDICAMENTO,
					oldDetalleMedicamento, detalleMedicamento));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void registrarMedicamento() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void editarMedicamento() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eliminarMedicamento() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ThreeClassAppPackage.MEDICAMENTO__ID_MEDICAMENTO:
			return getIdMedicamento();
		case ThreeClassAppPackage.MEDICAMENTO__COSTO_MEDICAMENTO:
			return getCostoMedicamento();
		case ThreeClassAppPackage.MEDICAMENTO__DETALLE_MEDICAMENTO:
			return getDetalleMedicamento();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ThreeClassAppPackage.MEDICAMENTO__ID_MEDICAMENTO:
			setIdMedicamento((Long) newValue);
			return;
		case ThreeClassAppPackage.MEDICAMENTO__COSTO_MEDICAMENTO:
			setCostoMedicamento((String) newValue);
			return;
		case ThreeClassAppPackage.MEDICAMENTO__DETALLE_MEDICAMENTO:
			setDetalleMedicamento((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ThreeClassAppPackage.MEDICAMENTO__ID_MEDICAMENTO:
			setIdMedicamento(ID_MEDICAMENTO_EDEFAULT);
			return;
		case ThreeClassAppPackage.MEDICAMENTO__COSTO_MEDICAMENTO:
			setCostoMedicamento(COSTO_MEDICAMENTO_EDEFAULT);
			return;
		case ThreeClassAppPackage.MEDICAMENTO__DETALLE_MEDICAMENTO:
			setDetalleMedicamento(DETALLE_MEDICAMENTO_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ThreeClassAppPackage.MEDICAMENTO__ID_MEDICAMENTO:
			return idMedicamento != ID_MEDICAMENTO_EDEFAULT;
		case ThreeClassAppPackage.MEDICAMENTO__COSTO_MEDICAMENTO:
			return COSTO_MEDICAMENTO_EDEFAULT == null ? costoMedicamento != null
					: !COSTO_MEDICAMENTO_EDEFAULT.equals(costoMedicamento);
		case ThreeClassAppPackage.MEDICAMENTO__DETALLE_MEDICAMENTO:
			return DETALLE_MEDICAMENTO_EDEFAULT == null ? detalleMedicamento != null
					: !DETALLE_MEDICAMENTO_EDEFAULT.equals(detalleMedicamento);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ThreeClassAppPackage.MEDICAMENTO___REGISTRAR_MEDICAMENTO:
			registrarMedicamento();
			return null;
		case ThreeClassAppPackage.MEDICAMENTO___EDITAR_MEDICAMENTO:
			editarMedicamento();
			return null;
		case ThreeClassAppPackage.MEDICAMENTO___ELIMINAR_MEDICAMENTO:
			eliminarMedicamento();
			return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (idMedicamento: ");
		result.append(idMedicamento);
		result.append(", costoMedicamento: ");
		result.append(costoMedicamento);
		result.append(", detalleMedicamento: ");
		result.append(detalleMedicamento);
		result.append(')');
		return result.toString();
	}

} //MedicamentoImpl
