/**
 */
package com.example.demo.threeClassApp.impl;

import com.example.demo.threeClassApp.Medicamento;
import com.example.demo.threeClassApp.ThreeClassAppPackage;
import com.example.demo.threeClassApp.Tratamiento;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tratamiento</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.example.demo.threeClassApp.impl.TratamientoImpl#getIdTratamiento <em>Id Tratamiento</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.impl.TratamientoImpl#getDescripcionTratamiento <em>Descripcion Tratamiento</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.impl.TratamientoImpl#getTiene <em>Tiene</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TratamientoImpl extends MinimalEObjectImpl.Container implements Tratamiento {
	/**
	 * The default value of the '{@link #getIdTratamiento() <em>Id Tratamiento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdTratamiento()
	 * @generated
	 * @ordered
	 */
	protected static final long ID_TRATAMIENTO_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getIdTratamiento() <em>Id Tratamiento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdTratamiento()
	 * @generated
	 * @ordered
	 */
	protected long idTratamiento = ID_TRATAMIENTO_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescripcionTratamiento() <em>Descripcion Tratamiento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescripcionTratamiento()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPCION_TRATAMIENTO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescripcionTratamiento() <em>Descripcion Tratamiento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescripcionTratamiento()
	 * @generated
	 * @ordered
	 */
	protected String descripcionTratamiento = DESCRIPCION_TRATAMIENTO_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTiene() <em>Tiene</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTiene()
	 * @generated
	 * @ordered
	 */
	protected EList<Medicamento> tiene;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TratamientoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ThreeClassAppPackage.Literals.TRATAMIENTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getIdTratamiento() {
		return idTratamiento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdTratamiento(long newIdTratamiento) {
		long oldIdTratamiento = idTratamiento;
		idTratamiento = newIdTratamiento;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreeClassAppPackage.TRATAMIENTO__ID_TRATAMIENTO,
					oldIdTratamiento, idTratamiento));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescripcionTratamiento() {
		return descripcionTratamiento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescripcionTratamiento(String newDescripcionTratamiento) {
		String oldDescripcionTratamiento = descripcionTratamiento;
		descripcionTratamiento = newDescripcionTratamiento;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ThreeClassAppPackage.TRATAMIENTO__DESCRIPCION_TRATAMIENTO, oldDescripcionTratamiento,
					descripcionTratamiento));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Medicamento> getTiene() {
		if (tiene == null) {
			tiene = new EObjectResolvingEList<Medicamento>(Medicamento.class, this,
					ThreeClassAppPackage.TRATAMIENTO__TIENE);
		}
		return tiene;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void realizarDiagnostico() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void verTratamiento() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ThreeClassAppPackage.TRATAMIENTO__ID_TRATAMIENTO:
			return getIdTratamiento();
		case ThreeClassAppPackage.TRATAMIENTO__DESCRIPCION_TRATAMIENTO:
			return getDescripcionTratamiento();
		case ThreeClassAppPackage.TRATAMIENTO__TIENE:
			return getTiene();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ThreeClassAppPackage.TRATAMIENTO__ID_TRATAMIENTO:
			setIdTratamiento((Long) newValue);
			return;
		case ThreeClassAppPackage.TRATAMIENTO__DESCRIPCION_TRATAMIENTO:
			setDescripcionTratamiento((String) newValue);
			return;
		case ThreeClassAppPackage.TRATAMIENTO__TIENE:
			getTiene().clear();
			getTiene().addAll((Collection<? extends Medicamento>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ThreeClassAppPackage.TRATAMIENTO__ID_TRATAMIENTO:
			setIdTratamiento(ID_TRATAMIENTO_EDEFAULT);
			return;
		case ThreeClassAppPackage.TRATAMIENTO__DESCRIPCION_TRATAMIENTO:
			setDescripcionTratamiento(DESCRIPCION_TRATAMIENTO_EDEFAULT);
			return;
		case ThreeClassAppPackage.TRATAMIENTO__TIENE:
			getTiene().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ThreeClassAppPackage.TRATAMIENTO__ID_TRATAMIENTO:
			return idTratamiento != ID_TRATAMIENTO_EDEFAULT;
		case ThreeClassAppPackage.TRATAMIENTO__DESCRIPCION_TRATAMIENTO:
			return DESCRIPCION_TRATAMIENTO_EDEFAULT == null ? descripcionTratamiento != null
					: !DESCRIPCION_TRATAMIENTO_EDEFAULT.equals(descripcionTratamiento);
		case ThreeClassAppPackage.TRATAMIENTO__TIENE:
			return tiene != null && !tiene.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ThreeClassAppPackage.TRATAMIENTO___REALIZAR_DIAGNOSTICO:
			realizarDiagnostico();
			return null;
		case ThreeClassAppPackage.TRATAMIENTO___VER_TRATAMIENTO:
			verTratamiento();
			return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (idTratamiento: ");
		result.append(idTratamiento);
		result.append(", descripcionTratamiento: ");
		result.append(descripcionTratamiento);
		result.append(')');
		return result.toString();
	}

} //TratamientoImpl
