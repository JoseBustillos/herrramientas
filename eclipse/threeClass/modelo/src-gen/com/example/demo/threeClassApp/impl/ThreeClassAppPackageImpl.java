/**
 */
package com.example.demo.threeClassApp.impl;

import com.example.demo.threeClassApp.Medicamento;
import com.example.demo.threeClassApp.Prescripcion;
import com.example.demo.threeClassApp.ThreeClassAppFactory;
import com.example.demo.threeClassApp.ThreeClassAppPackage;
import com.example.demo.threeClassApp.Tratamiento;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ThreeClassAppPackageImpl extends EPackageImpl implements ThreeClassAppPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass prescripcionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass medicamentoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tratamientoEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ThreeClassAppPackageImpl() {
		super(eNS_URI, ThreeClassAppFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ThreeClassAppPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ThreeClassAppPackage init() {
		if (isInited)
			return (ThreeClassAppPackage) EPackage.Registry.INSTANCE.getEPackage(ThreeClassAppPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredThreeClassAppPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ThreeClassAppPackageImpl theThreeClassAppPackage = registeredThreeClassAppPackage instanceof ThreeClassAppPackageImpl
				? (ThreeClassAppPackageImpl) registeredThreeClassAppPackage
				: new ThreeClassAppPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theThreeClassAppPackage.createPackageContents();

		// Initialize created meta-data
		theThreeClassAppPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theThreeClassAppPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ThreeClassAppPackage.eNS_URI, theThreeClassAppPackage);
		return theThreeClassAppPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrescripcion() {
		return prescripcionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPrescripcion_IdPrescripcion() {
		return (EAttribute) prescripcionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPrescripcion_Detalle() {
		return (EAttribute) prescripcionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrescripcion_Pertenece() {
		return (EReference) prescripcionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPrescripcion__RegistrarPrescripcion() {
		return prescripcionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPrescripcion__VerPrescripcion() {
		return prescripcionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMedicamento() {
		return medicamentoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMedicamento_IdMedicamento() {
		return (EAttribute) medicamentoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMedicamento_CostoMedicamento() {
		return (EAttribute) medicamentoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMedicamento_DetalleMedicamento() {
		return (EAttribute) medicamentoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMedicamento__RegistrarMedicamento() {
		return medicamentoEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMedicamento__EditarMedicamento() {
		return medicamentoEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMedicamento__EliminarMedicamento() {
		return medicamentoEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTratamiento() {
		return tratamientoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTratamiento_IdTratamiento() {
		return (EAttribute) tratamientoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTratamiento_DescripcionTratamiento() {
		return (EAttribute) tratamientoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTratamiento_Tiene() {
		return (EReference) tratamientoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTratamiento__RealizarDiagnostico() {
		return tratamientoEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTratamiento__VerTratamiento() {
		return tratamientoEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThreeClassAppFactory getThreeClassAppFactory() {
		return (ThreeClassAppFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		prescripcionEClass = createEClass(PRESCRIPCION);
		createEAttribute(prescripcionEClass, PRESCRIPCION__ID_PRESCRIPCION);
		createEAttribute(prescripcionEClass, PRESCRIPCION__DETALLE);
		createEReference(prescripcionEClass, PRESCRIPCION__PERTENECE);
		createEOperation(prescripcionEClass, PRESCRIPCION___REGISTRAR_PRESCRIPCION);
		createEOperation(prescripcionEClass, PRESCRIPCION___VER_PRESCRIPCION);

		medicamentoEClass = createEClass(MEDICAMENTO);
		createEAttribute(medicamentoEClass, MEDICAMENTO__ID_MEDICAMENTO);
		createEAttribute(medicamentoEClass, MEDICAMENTO__COSTO_MEDICAMENTO);
		createEAttribute(medicamentoEClass, MEDICAMENTO__DETALLE_MEDICAMENTO);
		createEOperation(medicamentoEClass, MEDICAMENTO___REGISTRAR_MEDICAMENTO);
		createEOperation(medicamentoEClass, MEDICAMENTO___EDITAR_MEDICAMENTO);
		createEOperation(medicamentoEClass, MEDICAMENTO___ELIMINAR_MEDICAMENTO);

		tratamientoEClass = createEClass(TRATAMIENTO);
		createEAttribute(tratamientoEClass, TRATAMIENTO__ID_TRATAMIENTO);
		createEAttribute(tratamientoEClass, TRATAMIENTO__DESCRIPCION_TRATAMIENTO);
		createEReference(tratamientoEClass, TRATAMIENTO__TIENE);
		createEOperation(tratamientoEClass, TRATAMIENTO___REALIZAR_DIAGNOSTICO);
		createEOperation(tratamientoEClass, TRATAMIENTO___VER_TRATAMIENTO);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(prescripcionEClass, Prescripcion.class, "Prescripcion", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPrescripcion_IdPrescripcion(), ecorePackage.getELong(), "idPrescripcion", null, 0, 1,
				Prescripcion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPrescripcion_Detalle(), ecorePackage.getEString(), "detalle", null, 0, 1, Prescripcion.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPrescripcion_Pertenece(), this.getMedicamento(), null, "pertenece", null, 1, -1,
				Prescripcion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getPrescripcion__RegistrarPrescripcion(), null, "registrarPrescripcion", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getPrescripcion__VerPrescripcion(), null, "verPrescripcion", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(medicamentoEClass, Medicamento.class, "Medicamento", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMedicamento_IdMedicamento(), ecorePackage.getELong(), "idMedicamento", null, 0, 1,
				Medicamento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMedicamento_CostoMedicamento(), ecorePackage.getEString(), "costoMedicamento", null, 0, 1,
				Medicamento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMedicamento_DetalleMedicamento(), ecorePackage.getEString(), "detalleMedicamento", null, 0, 1,
				Medicamento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEOperation(getMedicamento__RegistrarMedicamento(), null, "registrarMedicamento", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMedicamento__EditarMedicamento(), null, "editarMedicamento", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMedicamento__EliminarMedicamento(), null, "eliminarMedicamento", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(tratamientoEClass, Tratamiento.class, "Tratamiento", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTratamiento_IdTratamiento(), ecorePackage.getELong(), "idTratamiento", null, 0, 1,
				Tratamiento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getTratamiento_DescripcionTratamiento(), ecorePackage.getEString(), "descripcionTratamiento",
				null, 0, 1, Tratamiento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTratamiento_Tiene(), this.getMedicamento(), null, "tiene", null, 1, -1, Tratamiento.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getTratamiento__RealizarDiagnostico(), null, "realizarDiagnostico", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getTratamiento__VerTratamiento(), null, "verTratamiento", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ThreeClassAppPackageImpl
