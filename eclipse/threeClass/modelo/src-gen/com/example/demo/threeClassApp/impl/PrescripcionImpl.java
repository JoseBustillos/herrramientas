/**
 */
package com.example.demo.threeClassApp.impl;

import com.example.demo.threeClassApp.Medicamento;
import com.example.demo.threeClassApp.Prescripcion;
import com.example.demo.threeClassApp.ThreeClassAppPackage;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Prescripcion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.example.demo.threeClassApp.impl.PrescripcionImpl#getIdPrescripcion <em>Id Prescripcion</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.impl.PrescripcionImpl#getDetalle <em>Detalle</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.impl.PrescripcionImpl#getPertenece <em>Pertenece</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PrescripcionImpl extends MinimalEObjectImpl.Container implements Prescripcion {
	/**
	 * The default value of the '{@link #getIdPrescripcion() <em>Id Prescripcion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdPrescripcion()
	 * @generated
	 * @ordered
	 */
	protected static final long ID_PRESCRIPCION_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getIdPrescripcion() <em>Id Prescripcion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdPrescripcion()
	 * @generated
	 * @ordered
	 */
	protected long idPrescripcion = ID_PRESCRIPCION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDetalle() <em>Detalle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetalle()
	 * @generated
	 * @ordered
	 */
	protected static final String DETALLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDetalle() <em>Detalle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetalle()
	 * @generated
	 * @ordered
	 */
	protected String detalle = DETALLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPertenece() <em>Pertenece</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPertenece()
	 * @generated
	 * @ordered
	 */
	protected EList<Medicamento> pertenece;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrescripcionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ThreeClassAppPackage.Literals.PRESCRIPCION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getIdPrescripcion() {
		return idPrescripcion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdPrescripcion(long newIdPrescripcion) {
		long oldIdPrescripcion = idPrescripcion;
		idPrescripcion = newIdPrescripcion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreeClassAppPackage.PRESCRIPCION__ID_PRESCRIPCION,
					oldIdPrescripcion, idPrescripcion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDetalle() {
		return detalle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDetalle(String newDetalle) {
		String oldDetalle = detalle;
		detalle = newDetalle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreeClassAppPackage.PRESCRIPCION__DETALLE,
					oldDetalle, detalle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Medicamento> getPertenece() {
		if (pertenece == null) {
			pertenece = new EObjectContainmentEList<Medicamento>(Medicamento.class, this,
					ThreeClassAppPackage.PRESCRIPCION__PERTENECE);
		}
		return pertenece;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void registrarPrescripcion() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void verPrescripcion() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ThreeClassAppPackage.PRESCRIPCION__PERTENECE:
			return ((InternalEList<?>) getPertenece()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ThreeClassAppPackage.PRESCRIPCION__ID_PRESCRIPCION:
			return getIdPrescripcion();
		case ThreeClassAppPackage.PRESCRIPCION__DETALLE:
			return getDetalle();
		case ThreeClassAppPackage.PRESCRIPCION__PERTENECE:
			return getPertenece();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ThreeClassAppPackage.PRESCRIPCION__ID_PRESCRIPCION:
			setIdPrescripcion((Long) newValue);
			return;
		case ThreeClassAppPackage.PRESCRIPCION__DETALLE:
			setDetalle((String) newValue);
			return;
		case ThreeClassAppPackage.PRESCRIPCION__PERTENECE:
			getPertenece().clear();
			getPertenece().addAll((Collection<? extends Medicamento>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ThreeClassAppPackage.PRESCRIPCION__ID_PRESCRIPCION:
			setIdPrescripcion(ID_PRESCRIPCION_EDEFAULT);
			return;
		case ThreeClassAppPackage.PRESCRIPCION__DETALLE:
			setDetalle(DETALLE_EDEFAULT);
			return;
		case ThreeClassAppPackage.PRESCRIPCION__PERTENECE:
			getPertenece().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ThreeClassAppPackage.PRESCRIPCION__ID_PRESCRIPCION:
			return idPrescripcion != ID_PRESCRIPCION_EDEFAULT;
		case ThreeClassAppPackage.PRESCRIPCION__DETALLE:
			return DETALLE_EDEFAULT == null ? detalle != null : !DETALLE_EDEFAULT.equals(detalle);
		case ThreeClassAppPackage.PRESCRIPCION__PERTENECE:
			return pertenece != null && !pertenece.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ThreeClassAppPackage.PRESCRIPCION___REGISTRAR_PRESCRIPCION:
			registrarPrescripcion();
			return null;
		case ThreeClassAppPackage.PRESCRIPCION___VER_PRESCRIPCION:
			verPrescripcion();
			return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (idPrescripcion: ");
		result.append(idPrescripcion);
		result.append(", detalle: ");
		result.append(detalle);
		result.append(')');
		return result.toString();
	}

} //PrescripcionImpl
