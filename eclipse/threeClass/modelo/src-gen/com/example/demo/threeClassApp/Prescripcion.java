/**
 */
package com.example.demo.threeClassApp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Prescripcion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.example.demo.threeClassApp.Prescripcion#getIdPrescripcion <em>Id Prescripcion</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.Prescripcion#getDetalle <em>Detalle</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.Prescripcion#getPertenece <em>Pertenece</em>}</li>
 * </ul>
 *
 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getPrescripcion()
 * @model
 * @generated
 */
public interface Prescripcion extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Prescripcion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Prescripcion</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Prescripcion</em>' attribute.
	 * @see #setIdPrescripcion(long)
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getPrescripcion_IdPrescripcion()
	 * @model
	 * @generated
	 */
	long getIdPrescripcion();

	/**
	 * Sets the value of the '{@link com.example.demo.threeClassApp.Prescripcion#getIdPrescripcion <em>Id Prescripcion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Prescripcion</em>' attribute.
	 * @see #getIdPrescripcion()
	 * @generated
	 */
	void setIdPrescripcion(long value);

	/**
	 * Returns the value of the '<em><b>Detalle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detalle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detalle</em>' attribute.
	 * @see #setDetalle(String)
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getPrescripcion_Detalle()
	 * @model
	 * @generated
	 */
	String getDetalle();

	/**
	 * Sets the value of the '{@link com.example.demo.threeClassApp.Prescripcion#getDetalle <em>Detalle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detalle</em>' attribute.
	 * @see #getDetalle()
	 * @generated
	 */
	void setDetalle(String value);

	/**
	 * Returns the value of the '<em><b>Pertenece</b></em>' containment reference list.
	 * The list contents are of type {@link com.example.demo.threeClassApp.Medicamento}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pertenece</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pertenece</em>' containment reference list.
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getPrescripcion_Pertenece()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Medicamento> getPertenece();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void registrarPrescripcion();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void verPrescripcion();

} // Prescripcion
