/**
 */
package com.example.demo.threeClassApp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Medicamento</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.example.demo.threeClassApp.Medicamento#getIdMedicamento <em>Id Medicamento</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.Medicamento#getCostoMedicamento <em>Costo Medicamento</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.Medicamento#getDetalleMedicamento <em>Detalle Medicamento</em>}</li>
 * </ul>
 *
 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getMedicamento()
 * @model
 * @generated
 */
public interface Medicamento extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Medicamento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Medicamento</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Medicamento</em>' attribute.
	 * @see #setIdMedicamento(long)
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getMedicamento_IdMedicamento()
	 * @model
	 * @generated
	 */
	long getIdMedicamento();

	/**
	 * Sets the value of the '{@link com.example.demo.threeClassApp.Medicamento#getIdMedicamento <em>Id Medicamento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Medicamento</em>' attribute.
	 * @see #getIdMedicamento()
	 * @generated
	 */
	void setIdMedicamento(long value);

	/**
	 * Returns the value of the '<em><b>Costo Medicamento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Costo Medicamento</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Costo Medicamento</em>' attribute.
	 * @see #setCostoMedicamento(String)
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getMedicamento_CostoMedicamento()
	 * @model
	 * @generated
	 */
	String getCostoMedicamento();

	/**
	 * Sets the value of the '{@link com.example.demo.threeClassApp.Medicamento#getCostoMedicamento <em>Costo Medicamento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Costo Medicamento</em>' attribute.
	 * @see #getCostoMedicamento()
	 * @generated
	 */
	void setCostoMedicamento(String value);

	/**
	 * Returns the value of the '<em><b>Detalle Medicamento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detalle Medicamento</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detalle Medicamento</em>' attribute.
	 * @see #setDetalleMedicamento(String)
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getMedicamento_DetalleMedicamento()
	 * @model
	 * @generated
	 */
	String getDetalleMedicamento();

	/**
	 * Sets the value of the '{@link com.example.demo.threeClassApp.Medicamento#getDetalleMedicamento <em>Detalle Medicamento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detalle Medicamento</em>' attribute.
	 * @see #getDetalleMedicamento()
	 * @generated
	 */
	void setDetalleMedicamento(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void registrarMedicamento();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void editarMedicamento();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void eliminarMedicamento();

} // Medicamento
