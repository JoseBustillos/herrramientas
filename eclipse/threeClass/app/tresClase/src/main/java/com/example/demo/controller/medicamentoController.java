package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.repository.medicamentoRepository;
import com.example.demo.threeClassApp.impl.MedicamentoImpl;
import com.example.demo.threeClassApp.impl.PrescripcionImpl;
import com.example.demo.threeClassApp.impl.TratamientoImpl;

@RestController
//defecto -> localhost:8080
//localhost:8080/api/v1
@RequestMapping("/api/v1")
public class medicamentoController {
	@Autowired
	private medicamentoRepository medicamentoRepository;
	
	// localhost:8080/api/v1/medicamentos
	@GetMapping("/medicamento")
	public List<MedicamentoImpl> getAllMedicamentos(){
		return medicamentoRepository.findAll();
	}
	
	/*,
					tratamientoRepository.findById(tratamientoId)
					.map(tratamiento ->{medicamento.setTratamiento(tratamiento)*/
	@PostMapping("/medicamento")
	public MedicamentoImpl createMedicamento(
			@ModelAttribute MedicamentoImpl medicamento,
			@ModelAttribute TratamientoImpl tratamiento,
			@ModelAttribute PrescripcionImpl prescripcion) {
		medicamento.setTratamiento(tratamiento);
		medicamento.setPrescripcion(prescripcion);
	    return medicamentoRepository.save(medicamento);
	}
	
	
}
