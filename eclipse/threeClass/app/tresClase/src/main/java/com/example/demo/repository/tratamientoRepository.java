package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.threeClassApp.impl.TratamientoImpl;

@Repository
public interface tratamientoRepository extends JpaRepository<TratamientoImpl, Long>{
}
