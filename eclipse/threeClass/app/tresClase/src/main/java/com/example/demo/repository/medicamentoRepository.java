package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.threeClassApp.impl.MedicamentoImpl;

@Repository
public interface medicamentoRepository extends JpaRepository<MedicamentoImpl, Long>{
	//List<TratamientoImpl> findByfindByMedicamento(MedicamentoImpl medicamentoImpl, Sort sort);
	List<MedicamentoImpl> findByTratamiento(Long tratamientoId);
	List<MedicamentoImpl> findByPrescripcion(Long prescripcionId);

}
