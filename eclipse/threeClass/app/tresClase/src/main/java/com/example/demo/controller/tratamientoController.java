package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.threeClassApp.impl.TratamientoImpl;
import com.example.demo.repository.tratamientoRepository;
	
@RestController
// defecto -> localhost:8080
// localhost:8080/api/v1
@RequestMapping("/api/v1")
public class tratamientoController {
	@Autowired
	private tratamientoRepository tratamientoRepository;
	
	// localhost:8080/api/v1/tratamiento
	@GetMapping("/tratamiento")
	public List<TratamientoImpl> getAllTratamientos(){
		return tratamientoRepository.findAll();
	}
	
	// localhost:8080/api/v1/tratamiento
	@PostMapping("/tratamiento")
	public TratamientoImpl createTratamientoImpl(@ModelAttribute TratamientoImpl tratamiento){
		return tratamientoRepository.save(tratamiento);
	}

}


