/**
 */
package com.example.demo.threeClassApp;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.example.demo.threeClassApp.ThreeClassAppFactory
 * @model kind="package"
 * @generated
 */
public interface ThreeClassAppPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "threeClassApp";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/threeClassApp";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "threeClassApp";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ThreeClassAppPackage eINSTANCE = com.example.demo.threeClassApp.impl.ThreeClassAppPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.example.demo.threeClassApp.impl.PrescripcionImpl <em>Prescripcion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.example.demo.threeClassApp.impl.PrescripcionImpl
	 * @see com.example.demo.threeClassApp.impl.ThreeClassAppPackageImpl#getPrescripcion()
	 * @generated
	 */
	int PRESCRIPCION = 0;

	/**
	 * The feature id for the '<em><b>Id Prescripcion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESCRIPCION__ID_PRESCRIPCION = 0;

	/**
	 * The feature id for the '<em><b>Detalle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESCRIPCION__DETALLE = 1;

	/**
	 * The feature id for the '<em><b>Pertenece</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESCRIPCION__PERTENECE = 2;

	/**
	 * The number of structural features of the '<em>Prescripcion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESCRIPCION_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Registrar Prescripcion</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESCRIPCION___REGISTRAR_PRESCRIPCION = 0;

	/**
	 * The operation id for the '<em>Ver Prescripcion</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESCRIPCION___VER_PRESCRIPCION = 1;

	/**
	 * The number of operations of the '<em>Prescripcion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRESCRIPCION_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link com.example.demo.threeClassApp.impl.MedicamentoImpl <em>Medicamento</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.example.demo.threeClassApp.impl.MedicamentoImpl
	 * @see com.example.demo.threeClassApp.impl.ThreeClassAppPackageImpl#getMedicamento()
	 * @generated
	 */
	int MEDICAMENTO = 1;

	/**
	 * The feature id for the '<em><b>Id Medicamento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDICAMENTO__ID_MEDICAMENTO = 0;

	/**
	 * The feature id for the '<em><b>Costo Medicamento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDICAMENTO__COSTO_MEDICAMENTO = 1;

	/**
	 * The feature id for the '<em><b>Detalle Medicamento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDICAMENTO__DETALLE_MEDICAMENTO = 2;

	/**
	 * The number of structural features of the '<em>Medicamento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDICAMENTO_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Registrar Medicamento</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDICAMENTO___REGISTRAR_MEDICAMENTO = 0;

	/**
	 * The operation id for the '<em>Editar Medicamento</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDICAMENTO___EDITAR_MEDICAMENTO = 1;

	/**
	 * The operation id for the '<em>Eliminar Medicamento</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDICAMENTO___ELIMINAR_MEDICAMENTO = 2;

	/**
	 * The number of operations of the '<em>Medicamento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDICAMENTO_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link com.example.demo.threeClassApp.impl.TratamientoImpl <em>Tratamiento</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.example.demo.threeClassApp.impl.TratamientoImpl
	 * @see com.example.demo.threeClassApp.impl.ThreeClassAppPackageImpl#getTratamiento()
	 * @generated
	 */
	int TRATAMIENTO = 2;

	/**
	 * The feature id for the '<em><b>Id Tratamiento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRATAMIENTO__ID_TRATAMIENTO = 0;

	/**
	 * The feature id for the '<em><b>Descripcion Tratamiento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRATAMIENTO__DESCRIPCION_TRATAMIENTO = 1;

	/**
	 * The feature id for the '<em><b>Tiene</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRATAMIENTO__TIENE = 2;

	/**
	 * The number of structural features of the '<em>Tratamiento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRATAMIENTO_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Realizar Diagnostico</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRATAMIENTO___REALIZAR_DIAGNOSTICO = 0;

	/**
	 * The operation id for the '<em>Ver Tratamiento</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRATAMIENTO___VER_TRATAMIENTO = 1;

	/**
	 * The number of operations of the '<em>Tratamiento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRATAMIENTO_OPERATION_COUNT = 2;

	/**
	 * Returns the meta object for class '{@link com.example.demo.threeClassApp.Prescripcion <em>Prescripcion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Prescripcion</em>'.
	 * @see com.example.demo.threeClassApp.Prescripcion
	 * @generated
	 */
	EClass getPrescripcion();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.threeClassApp.Prescripcion#getIdPrescripcion <em>Id Prescripcion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Prescripcion</em>'.
	 * @see com.example.demo.threeClassApp.Prescripcion#getIdPrescripcion()
	 * @see #getPrescripcion()
	 * @generated
	 */
	EAttribute getPrescripcion_IdPrescripcion();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.threeClassApp.Prescripcion#getDetalle <em>Detalle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Detalle</em>'.
	 * @see com.example.demo.threeClassApp.Prescripcion#getDetalle()
	 * @see #getPrescripcion()
	 * @generated
	 */
	EAttribute getPrescripcion_Detalle();

	/**
	 * Returns the meta object for the containment reference list '{@link com.example.demo.threeClassApp.Prescripcion#getPertenece <em>Pertenece</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pertenece</em>'.
	 * @see com.example.demo.threeClassApp.Prescripcion#getPertenece()
	 * @see #getPrescripcion()
	 * @generated
	 */
	EReference getPrescripcion_Pertenece();

	/**
	 * Returns the meta object for the '{@link com.example.demo.threeClassApp.Prescripcion#registrarPrescripcion() <em>Registrar Prescripcion</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Registrar Prescripcion</em>' operation.
	 * @see com.example.demo.threeClassApp.Prescripcion#registrarPrescripcion()
	 * @generated
	 */
	EOperation getPrescripcion__RegistrarPrescripcion();

	/**
	 * Returns the meta object for the '{@link com.example.demo.threeClassApp.Prescripcion#verPrescripcion() <em>Ver Prescripcion</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ver Prescripcion</em>' operation.
	 * @see com.example.demo.threeClassApp.Prescripcion#verPrescripcion()
	 * @generated
	 */
	EOperation getPrescripcion__VerPrescripcion();

	/**
	 * Returns the meta object for class '{@link com.example.demo.threeClassApp.Medicamento <em>Medicamento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Medicamento</em>'.
	 * @see com.example.demo.threeClassApp.Medicamento
	 * @generated
	 */
	EClass getMedicamento();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.threeClassApp.Medicamento#getIdMedicamento <em>Id Medicamento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Medicamento</em>'.
	 * @see com.example.demo.threeClassApp.Medicamento#getIdMedicamento()
	 * @see #getMedicamento()
	 * @generated
	 */
	EAttribute getMedicamento_IdMedicamento();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.threeClassApp.Medicamento#getCostoMedicamento <em>Costo Medicamento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Costo Medicamento</em>'.
	 * @see com.example.demo.threeClassApp.Medicamento#getCostoMedicamento()
	 * @see #getMedicamento()
	 * @generated
	 */
	EAttribute getMedicamento_CostoMedicamento();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.threeClassApp.Medicamento#getDetalleMedicamento <em>Detalle Medicamento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Detalle Medicamento</em>'.
	 * @see com.example.demo.threeClassApp.Medicamento#getDetalleMedicamento()
	 * @see #getMedicamento()
	 * @generated
	 */
	EAttribute getMedicamento_DetalleMedicamento();

	/**
	 * Returns the meta object for the '{@link com.example.demo.threeClassApp.Medicamento#registrarMedicamento() <em>Registrar Medicamento</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Registrar Medicamento</em>' operation.
	 * @see com.example.demo.threeClassApp.Medicamento#registrarMedicamento()
	 * @generated
	 */
	EOperation getMedicamento__RegistrarMedicamento();

	/**
	 * Returns the meta object for the '{@link com.example.demo.threeClassApp.Medicamento#editarMedicamento() <em>Editar Medicamento</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Editar Medicamento</em>' operation.
	 * @see com.example.demo.threeClassApp.Medicamento#editarMedicamento()
	 * @generated
	 */
	EOperation getMedicamento__EditarMedicamento();

	/**
	 * Returns the meta object for the '{@link com.example.demo.threeClassApp.Medicamento#eliminarMedicamento() <em>Eliminar Medicamento</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Eliminar Medicamento</em>' operation.
	 * @see com.example.demo.threeClassApp.Medicamento#eliminarMedicamento()
	 * @generated
	 */
	EOperation getMedicamento__EliminarMedicamento();

	/**
	 * Returns the meta object for class '{@link com.example.demo.threeClassApp.Tratamiento <em>Tratamiento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tratamiento</em>'.
	 * @see com.example.demo.threeClassApp.Tratamiento
	 * @generated
	 */
	EClass getTratamiento();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.threeClassApp.Tratamiento#getIdTratamiento <em>Id Tratamiento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Tratamiento</em>'.
	 * @see com.example.demo.threeClassApp.Tratamiento#getIdTratamiento()
	 * @see #getTratamiento()
	 * @generated
	 */
	EAttribute getTratamiento_IdTratamiento();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.threeClassApp.Tratamiento#getDescripcionTratamiento <em>Descripcion Tratamiento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Descripcion Tratamiento</em>'.
	 * @see com.example.demo.threeClassApp.Tratamiento#getDescripcionTratamiento()
	 * @see #getTratamiento()
	 * @generated
	 */
	EAttribute getTratamiento_DescripcionTratamiento();

	/**
	 * Returns the meta object for the reference list '{@link com.example.demo.threeClassApp.Tratamiento#getTiene <em>Tiene</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Tiene</em>'.
	 * @see com.example.demo.threeClassApp.Tratamiento#getTiene()
	 * @see #getTratamiento()
	 * @generated
	 */
	EReference getTratamiento_Tiene();

	/**
	 * Returns the meta object for the '{@link com.example.demo.threeClassApp.Tratamiento#realizarDiagnostico() <em>Realizar Diagnostico</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Realizar Diagnostico</em>' operation.
	 * @see com.example.demo.threeClassApp.Tratamiento#realizarDiagnostico()
	 * @generated
	 */
	EOperation getTratamiento__RealizarDiagnostico();

	/**
	 * Returns the meta object for the '{@link com.example.demo.threeClassApp.Tratamiento#verTratamiento() <em>Ver Tratamiento</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ver Tratamiento</em>' operation.
	 * @see com.example.demo.threeClassApp.Tratamiento#verTratamiento()
	 * @generated
	 */
	EOperation getTratamiento__VerTratamiento();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ThreeClassAppFactory getThreeClassAppFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.example.demo.threeClassApp.impl.PrescripcionImpl <em>Prescripcion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.example.demo.threeClassApp.impl.PrescripcionImpl
		 * @see com.example.demo.threeClassApp.impl.ThreeClassAppPackageImpl#getPrescripcion()
		 * @generated
		 */
		EClass PRESCRIPCION = eINSTANCE.getPrescripcion();

		/**
		 * The meta object literal for the '<em><b>Id Prescripcion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRESCRIPCION__ID_PRESCRIPCION = eINSTANCE.getPrescripcion_IdPrescripcion();

		/**
		 * The meta object literal for the '<em><b>Detalle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRESCRIPCION__DETALLE = eINSTANCE.getPrescripcion_Detalle();

		/**
		 * The meta object literal for the '<em><b>Pertenece</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRESCRIPCION__PERTENECE = eINSTANCE.getPrescripcion_Pertenece();

		/**
		 * The meta object literal for the '<em><b>Registrar Prescripcion</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PRESCRIPCION___REGISTRAR_PRESCRIPCION = eINSTANCE.getPrescripcion__RegistrarPrescripcion();

		/**
		 * The meta object literal for the '<em><b>Ver Prescripcion</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PRESCRIPCION___VER_PRESCRIPCION = eINSTANCE.getPrescripcion__VerPrescripcion();

		/**
		 * The meta object literal for the '{@link com.example.demo.threeClassApp.impl.MedicamentoImpl <em>Medicamento</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.example.demo.threeClassApp.impl.MedicamentoImpl
		 * @see com.example.demo.threeClassApp.impl.ThreeClassAppPackageImpl#getMedicamento()
		 * @generated
		 */
		EClass MEDICAMENTO = eINSTANCE.getMedicamento();

		/**
		 * The meta object literal for the '<em><b>Id Medicamento</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEDICAMENTO__ID_MEDICAMENTO = eINSTANCE.getMedicamento_IdMedicamento();

		/**
		 * The meta object literal for the '<em><b>Costo Medicamento</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEDICAMENTO__COSTO_MEDICAMENTO = eINSTANCE.getMedicamento_CostoMedicamento();

		/**
		 * The meta object literal for the '<em><b>Detalle Medicamento</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEDICAMENTO__DETALLE_MEDICAMENTO = eINSTANCE.getMedicamento_DetalleMedicamento();

		/**
		 * The meta object literal for the '<em><b>Registrar Medicamento</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEDICAMENTO___REGISTRAR_MEDICAMENTO = eINSTANCE.getMedicamento__RegistrarMedicamento();

		/**
		 * The meta object literal for the '<em><b>Editar Medicamento</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEDICAMENTO___EDITAR_MEDICAMENTO = eINSTANCE.getMedicamento__EditarMedicamento();

		/**
		 * The meta object literal for the '<em><b>Eliminar Medicamento</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEDICAMENTO___ELIMINAR_MEDICAMENTO = eINSTANCE.getMedicamento__EliminarMedicamento();

		/**
		 * The meta object literal for the '{@link com.example.demo.threeClassApp.impl.TratamientoImpl <em>Tratamiento</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.example.demo.threeClassApp.impl.TratamientoImpl
		 * @see com.example.demo.threeClassApp.impl.ThreeClassAppPackageImpl#getTratamiento()
		 * @generated
		 */
		EClass TRATAMIENTO = eINSTANCE.getTratamiento();

		/**
		 * The meta object literal for the '<em><b>Id Tratamiento</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRATAMIENTO__ID_TRATAMIENTO = eINSTANCE.getTratamiento_IdTratamiento();

		/**
		 * The meta object literal for the '<em><b>Descripcion Tratamiento</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRATAMIENTO__DESCRIPCION_TRATAMIENTO = eINSTANCE.getTratamiento_DescripcionTratamiento();

		/**
		 * The meta object literal for the '<em><b>Tiene</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRATAMIENTO__TIENE = eINSTANCE.getTratamiento_Tiene();

		/**
		 * The meta object literal for the '<em><b>Realizar Diagnostico</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRATAMIENTO___REALIZAR_DIAGNOSTICO = eINSTANCE.getTratamiento__RealizarDiagnostico();

		/**
		 * The meta object literal for the '<em><b>Ver Tratamiento</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRATAMIENTO___VER_TRATAMIENTO = eINSTANCE.getTratamiento__VerTratamiento();

	}

} //ThreeClassAppPackage
