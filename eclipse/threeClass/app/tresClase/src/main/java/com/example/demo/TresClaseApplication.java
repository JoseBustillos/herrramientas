package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TresClaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(TresClaseApplication.class, args);
	}

}
