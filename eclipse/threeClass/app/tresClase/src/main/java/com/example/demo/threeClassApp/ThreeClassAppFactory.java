/**
 */
package com.example.demo.threeClassApp;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.example.demo.threeClassApp.ThreeClassAppPackage
 * @generated
 */
public interface ThreeClassAppFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ThreeClassAppFactory eINSTANCE = com.example.demo.threeClassApp.impl.ThreeClassAppFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Prescripcion</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Prescripcion</em>'.
	 * @generated
	 */
	Prescripcion createPrescripcion();

	/**
	 * Returns a new object of class '<em>Medicamento</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Medicamento</em>'.
	 * @generated
	 */
	Medicamento createMedicamento();

	/**
	 * Returns a new object of class '<em>Tratamiento</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tratamiento</em>'.
	 * @generated
	 */
	Tratamiento createTratamiento();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ThreeClassAppPackage getThreeClassAppPackage();

} //ThreeClassAppFactory
