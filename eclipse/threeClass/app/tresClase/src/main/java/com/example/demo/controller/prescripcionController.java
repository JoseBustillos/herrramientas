package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.repository.prescripcionRepository;
import com.example.demo.threeClassApp.impl.PrescripcionImpl;

@RestController
// defecto -> localhost:8080
// localhost:8080/api/v1
@RequestMapping("/api/v1")
public class prescripcionController {
	@Autowired
	private prescripcionRepository prescripcionRepository;

	// localhost:8080/api/v1/prescripcion
	@GetMapping("/prescripcion")
	public List<PrescripcionImpl> getAllPrescripciones(){
		return prescripcionRepository.findAll();
	}

	@PostMapping("/prescripcion")
	public PrescripcionImpl createPrescripcionImpl(@ModelAttribute PrescripcionImpl prescripcion) {
		return prescripcionRepository.save(prescripcion);
	}

}
