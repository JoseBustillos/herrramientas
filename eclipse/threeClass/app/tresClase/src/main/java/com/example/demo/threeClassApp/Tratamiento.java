/**
 */
package com.example.demo.threeClassApp;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import com.example.demo.threeClassApp.impl.MedicamentoImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tratamiento</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.example.demo.threeClassApp.Tratamiento#getIdTratamiento <em>Id Tratamiento</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.Tratamiento#getDescripcionTratamiento <em>Descripcion Tratamiento</em>}</li>
 *   <li>{@link com.example.demo.threeClassApp.Tratamiento#getTiene <em>Tiene</em>}</li>
 * </ul>
 *
 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getTratamiento()
 * @model
 * @generated
 */
public interface Tratamiento extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Tratamiento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Tratamiento</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Tratamiento</em>' attribute.
	 * @see #setIdTratamiento(long)
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getTratamiento_IdTratamiento()
	 * @model
	 * @generated
	 */
	long getIdTratamiento();

	/**
	 * Sets the value of the '{@link com.example.demo.threeClassApp.Tratamiento#getIdTratamiento <em>Id Tratamiento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Tratamiento</em>' attribute.
	 * @see #getIdTratamiento()
	 * @generated
	 */
	void setIdTratamiento(long value);

	/**
	 * Returns the value of the '<em><b>Descripcion Tratamiento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Descripcion Tratamiento</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Descripcion Tratamiento</em>' attribute.
	 * @see #setDescripcionTratamiento(String)
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getTratamiento_DescripcionTratamiento()
	 * @model
	 * @generated
	 */
	String getDescripcionTratamiento();

	/**
	 * Sets the value of the '{@link com.example.demo.threeClassApp.Tratamiento#getDescripcionTratamiento <em>Descripcion Tratamiento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Descripcion Tratamiento</em>' attribute.
	 * @see #getDescripcionTratamiento()
	 * @generated
	 */
	void setDescripcionTratamiento(String value);

	/**
	 * Returns the value of the '<em><b>Tiene</b></em>' reference list.
	 * The list contents are of type {@link com.example.demo.threeClassApp.Medicamento}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tiene</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tiene</em>' reference list.
	 * @see com.example.demo.threeClassApp.ThreeClassAppPackage#getTratamiento_Tiene()
	 * @model required="true"
	 * @generated
	 */
	List<MedicamentoImpl> getTiene();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void realizarDiagnostico();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void verTratamiento();

} // Tratamiento
