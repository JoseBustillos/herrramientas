/**
 */
package com.example.demo.oneClassApp;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.example.demo.oneClassApp.OneClassAppFactory
 * @model kind="package"
 * @generated
 */
public interface OneClassAppPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "oneClassApp";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/oneClassApp";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "oneClassApp";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OneClassAppPackage eINSTANCE = com.example.demo.oneClassApp.impl.OneClassAppPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.example.demo.oneClassApp.impl.PacienteImpl <em>Paciente</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.example.demo.oneClassApp.impl.PacienteImpl
	 * @see com.example.demo.oneClassApp.impl.OneClassAppPackageImpl#getPaciente()
	 * @generated
	 */
	int PACIENTE = 0;

	/**
	 * The feature id for the '<em><b>Id Paciente</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACIENTE__ID_PACIENTE = 0;

	/**
	 * The feature id for the '<em><b>Nombre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACIENTE__NOMBRE = 1;

	/**
	 * The feature id for the '<em><b>Apellido</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACIENTE__APELLIDO = 2;

	/**
	 * The feature id for the '<em><b>Edad</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACIENTE__EDAD = 3;

	/**
	 * The feature id for the '<em><b>Telefono</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACIENTE__TELEFONO = 4;

	/**
	 * The number of structural features of the '<em>Paciente</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACIENTE_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Agendar Cita</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACIENTE___AGENDAR_CITA = 0;

	/**
	 * The operation id for the '<em>Realizar Pago</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACIENTE___REALIZAR_PAGO = 1;

	/**
	 * The number of operations of the '<em>Paciente</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACIENTE_OPERATION_COUNT = 2;

	/**
	 * Returns the meta object for class '{@link com.example.demo.oneClassApp.Paciente <em>Paciente</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paciente</em>'.
	 * @see com.example.demo.oneClassApp.Paciente
	 * @generated
	 */
	EClass getPaciente();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.oneClassApp.Paciente#getIdPaciente <em>Id Paciente</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Paciente</em>'.
	 * @see com.example.demo.oneClassApp.Paciente#getIdPaciente()
	 * @see #getPaciente()
	 * @generated
	 */
	EAttribute getPaciente_IdPaciente();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.oneClassApp.Paciente#getNombre <em>Nombre</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nombre</em>'.
	 * @see com.example.demo.oneClassApp.Paciente#getNombre()
	 * @see #getPaciente()
	 * @generated
	 */
	EAttribute getPaciente_Nombre();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.oneClassApp.Paciente#getApellido <em>Apellido</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Apellido</em>'.
	 * @see com.example.demo.oneClassApp.Paciente#getApellido()
	 * @see #getPaciente()
	 * @generated
	 */
	EAttribute getPaciente_Apellido();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.oneClassApp.Paciente#getEdad <em>Edad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Edad</em>'.
	 * @see com.example.demo.oneClassApp.Paciente#getEdad()
	 * @see #getPaciente()
	 * @generated
	 */
	EAttribute getPaciente_Edad();

	/**
	 * Returns the meta object for the attribute '{@link com.example.demo.oneClassApp.Paciente#getTelefono <em>Telefono</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Telefono</em>'.
	 * @see com.example.demo.oneClassApp.Paciente#getTelefono()
	 * @see #getPaciente()
	 * @generated
	 */
	EAttribute getPaciente_Telefono();

	/**
	 * Returns the meta object for the '{@link com.example.demo.oneClassApp.Paciente#agendarCita() <em>Agendar Cita</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Agendar Cita</em>' operation.
	 * @see com.example.demo.oneClassApp.Paciente#agendarCita()
	 * @generated
	 */
	EOperation getPaciente__AgendarCita();

	/**
	 * Returns the meta object for the '{@link com.example.demo.oneClassApp.Paciente#realizarPago() <em>Realizar Pago</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Realizar Pago</em>' operation.
	 * @see com.example.demo.oneClassApp.Paciente#realizarPago()
	 * @generated
	 */
	EOperation getPaciente__RealizarPago();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OneClassAppFactory getOneClassAppFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.example.demo.oneClassApp.impl.PacienteImpl <em>Paciente</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.example.demo.oneClassApp.impl.PacienteImpl
		 * @see com.example.demo.oneClassApp.impl.OneClassAppPackageImpl#getPaciente()
		 * @generated
		 */
		EClass PACIENTE = eINSTANCE.getPaciente();

		/**
		 * The meta object literal for the '<em><b>Id Paciente</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACIENTE__ID_PACIENTE = eINSTANCE.getPaciente_IdPaciente();

		/**
		 * The meta object literal for the '<em><b>Nombre</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACIENTE__NOMBRE = eINSTANCE.getPaciente_Nombre();

		/**
		 * The meta object literal for the '<em><b>Apellido</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACIENTE__APELLIDO = eINSTANCE.getPaciente_Apellido();

		/**
		 * The meta object literal for the '<em><b>Edad</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACIENTE__EDAD = eINSTANCE.getPaciente_Edad();

		/**
		 * The meta object literal for the '<em><b>Telefono</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACIENTE__TELEFONO = eINSTANCE.getPaciente_Telefono();

		/**
		 * The meta object literal for the '<em><b>Agendar Cita</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PACIENTE___AGENDAR_CITA = eINSTANCE.getPaciente__AgendarCita();

		/**
		 * The meta object literal for the '<em><b>Realizar Pago</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PACIENTE___REALIZAR_PAGO = eINSTANCE.getPaciente__RealizarPago();

	}

} //OneClassAppPackage
