/**
 */
package com.example.demo.oneClassApp;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.example.demo.oneClassApp.OneClassAppPackage
 * @generated
 */
public interface OneClassAppFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OneClassAppFactory eINSTANCE = com.example.demo.oneClassApp.impl.OneClassAppFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Paciente</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Paciente</em>'.
	 * @generated
	 */
	Paciente createPaciente();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OneClassAppPackage getOneClassAppPackage();

} //OneClassAppFactory
