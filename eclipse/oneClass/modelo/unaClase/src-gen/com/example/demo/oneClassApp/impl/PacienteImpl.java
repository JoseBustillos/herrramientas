/**
 */
package com.example.demo.oneClassApp.impl;

import com.example.demo.oneClassApp.OneClassAppPackage;
import com.example.demo.oneClassApp.Paciente;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Paciente</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.example.demo.oneClassApp.impl.PacienteImpl#getIdPaciente <em>Id Paciente</em>}</li>
 *   <li>{@link com.example.demo.oneClassApp.impl.PacienteImpl#getNombre <em>Nombre</em>}</li>
 *   <li>{@link com.example.demo.oneClassApp.impl.PacienteImpl#getApellido <em>Apellido</em>}</li>
 *   <li>{@link com.example.demo.oneClassApp.impl.PacienteImpl#getEdad <em>Edad</em>}</li>
 *   <li>{@link com.example.demo.oneClassApp.impl.PacienteImpl#getTelefono <em>Telefono</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PacienteImpl extends MinimalEObjectImpl.Container implements Paciente {
	/**
	 * The default value of the '{@link #getIdPaciente() <em>Id Paciente</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdPaciente()
	 * @generated
	 * @ordered
	 */
	protected static final long ID_PACIENTE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getIdPaciente() <em>Id Paciente</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdPaciente()
	 * @generated
	 * @ordered
	 */
	protected long idPaciente = ID_PACIENTE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNombre() <em>Nombre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNombre()
	 * @generated
	 * @ordered
	 */
	protected static final String NOMBRE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNombre() <em>Nombre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNombre()
	 * @generated
	 * @ordered
	 */
	protected String nombre = NOMBRE_EDEFAULT;

	/**
	 * The default value of the '{@link #getApellido() <em>Apellido</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApellido()
	 * @generated
	 * @ordered
	 */
	protected static final String APELLIDO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getApellido() <em>Apellido</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApellido()
	 * @generated
	 * @ordered
	 */
	protected String apellido = APELLIDO_EDEFAULT;

	/**
	 * The default value of the '{@link #getEdad() <em>Edad</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdad()
	 * @generated
	 * @ordered
	 */
	protected static final int EDAD_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEdad() <em>Edad</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdad()
	 * @generated
	 * @ordered
	 */
	protected int edad = EDAD_EDEFAULT;

	/**
	 * The default value of the '{@link #getTelefono() <em>Telefono</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTelefono()
	 * @generated
	 * @ordered
	 */
	protected static final String TELEFONO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTelefono() <em>Telefono</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTelefono()
	 * @generated
	 * @ordered
	 */
	protected String telefono = TELEFONO_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PacienteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OneClassAppPackage.Literals.PACIENTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getIdPaciente() {
		return idPaciente;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdPaciente(long newIdPaciente) {
		long oldIdPaciente = idPaciente;
		idPaciente = newIdPaciente;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OneClassAppPackage.PACIENTE__ID_PACIENTE,
					oldIdPaciente, idPaciente));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNombre(String newNombre) {
		String oldNombre = nombre;
		nombre = newNombre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OneClassAppPackage.PACIENTE__NOMBRE, oldNombre,
					nombre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApellido(String newApellido) {
		String oldApellido = apellido;
		apellido = newApellido;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OneClassAppPackage.PACIENTE__APELLIDO, oldApellido,
					apellido));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEdad() {
		return edad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEdad(int newEdad) {
		int oldEdad = edad;
		edad = newEdad;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OneClassAppPackage.PACIENTE__EDAD, oldEdad, edad));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTelefono(String newTelefono) {
		String oldTelefono = telefono;
		telefono = newTelefono;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OneClassAppPackage.PACIENTE__TELEFONO, oldTelefono,
					telefono));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void agendarCita() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void realizarPago() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case OneClassAppPackage.PACIENTE__ID_PACIENTE:
			return getIdPaciente();
		case OneClassAppPackage.PACIENTE__NOMBRE:
			return getNombre();
		case OneClassAppPackage.PACIENTE__APELLIDO:
			return getApellido();
		case OneClassAppPackage.PACIENTE__EDAD:
			return getEdad();
		case OneClassAppPackage.PACIENTE__TELEFONO:
			return getTelefono();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case OneClassAppPackage.PACIENTE__ID_PACIENTE:
			setIdPaciente((Long) newValue);
			return;
		case OneClassAppPackage.PACIENTE__NOMBRE:
			setNombre((String) newValue);
			return;
		case OneClassAppPackage.PACIENTE__APELLIDO:
			setApellido((String) newValue);
			return;
		case OneClassAppPackage.PACIENTE__EDAD:
			setEdad((Integer) newValue);
			return;
		case OneClassAppPackage.PACIENTE__TELEFONO:
			setTelefono((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case OneClassAppPackage.PACIENTE__ID_PACIENTE:
			setIdPaciente(ID_PACIENTE_EDEFAULT);
			return;
		case OneClassAppPackage.PACIENTE__NOMBRE:
			setNombre(NOMBRE_EDEFAULT);
			return;
		case OneClassAppPackage.PACIENTE__APELLIDO:
			setApellido(APELLIDO_EDEFAULT);
			return;
		case OneClassAppPackage.PACIENTE__EDAD:
			setEdad(EDAD_EDEFAULT);
			return;
		case OneClassAppPackage.PACIENTE__TELEFONO:
			setTelefono(TELEFONO_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case OneClassAppPackage.PACIENTE__ID_PACIENTE:
			return idPaciente != ID_PACIENTE_EDEFAULT;
		case OneClassAppPackage.PACIENTE__NOMBRE:
			return NOMBRE_EDEFAULT == null ? nombre != null : !NOMBRE_EDEFAULT.equals(nombre);
		case OneClassAppPackage.PACIENTE__APELLIDO:
			return APELLIDO_EDEFAULT == null ? apellido != null : !APELLIDO_EDEFAULT.equals(apellido);
		case OneClassAppPackage.PACIENTE__EDAD:
			return edad != EDAD_EDEFAULT;
		case OneClassAppPackage.PACIENTE__TELEFONO:
			return TELEFONO_EDEFAULT == null ? telefono != null : !TELEFONO_EDEFAULT.equals(telefono);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case OneClassAppPackage.PACIENTE___AGENDAR_CITA:
			agendarCita();
			return null;
		case OneClassAppPackage.PACIENTE___REALIZAR_PAGO:
			realizarPago();
			return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (idPaciente: ");
		result.append(idPaciente);
		result.append(", nombre: ");
		result.append(nombre);
		result.append(", apellido: ");
		result.append(apellido);
		result.append(", edad: ");
		result.append(edad);
		result.append(", telefono: ");
		result.append(telefono);
		result.append(')');
		return result.toString();
	}

} //PacienteImpl
