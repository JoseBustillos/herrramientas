/**
 */
package com.example.demo.oneClassApp.impl;

import com.example.demo.oneClassApp.OneClassAppFactory;
import com.example.demo.oneClassApp.OneClassAppPackage;
import com.example.demo.oneClassApp.Paciente;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OneClassAppPackageImpl extends EPackageImpl implements OneClassAppPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pacienteEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.example.demo.oneClassApp.OneClassAppPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OneClassAppPackageImpl() {
		super(eNS_URI, OneClassAppFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OneClassAppPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OneClassAppPackage init() {
		if (isInited)
			return (OneClassAppPackage) EPackage.Registry.INSTANCE.getEPackage(OneClassAppPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOneClassAppPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OneClassAppPackageImpl theOneClassAppPackage = registeredOneClassAppPackage instanceof OneClassAppPackageImpl
				? (OneClassAppPackageImpl) registeredOneClassAppPackage
				: new OneClassAppPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theOneClassAppPackage.createPackageContents();

		// Initialize created meta-data
		theOneClassAppPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOneClassAppPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OneClassAppPackage.eNS_URI, theOneClassAppPackage);
		return theOneClassAppPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPaciente() {
		return pacienteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPaciente_IdPaciente() {
		return (EAttribute) pacienteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPaciente_Nombre() {
		return (EAttribute) pacienteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPaciente_Apellido() {
		return (EAttribute) pacienteEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPaciente_Edad() {
		return (EAttribute) pacienteEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPaciente_Telefono() {
		return (EAttribute) pacienteEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPaciente__AgendarCita() {
		return pacienteEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPaciente__RealizarPago() {
		return pacienteEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OneClassAppFactory getOneClassAppFactory() {
		return (OneClassAppFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		pacienteEClass = createEClass(PACIENTE);
		createEAttribute(pacienteEClass, PACIENTE__ID_PACIENTE);
		createEAttribute(pacienteEClass, PACIENTE__NOMBRE);
		createEAttribute(pacienteEClass, PACIENTE__APELLIDO);
		createEAttribute(pacienteEClass, PACIENTE__EDAD);
		createEAttribute(pacienteEClass, PACIENTE__TELEFONO);
		createEOperation(pacienteEClass, PACIENTE___AGENDAR_CITA);
		createEOperation(pacienteEClass, PACIENTE___REALIZAR_PAGO);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(pacienteEClass, Paciente.class, "Paciente", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPaciente_IdPaciente(), ecorePackage.getELong(), "idPaciente", null, 0, 1, Paciente.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPaciente_Nombre(), ecorePackage.getEString(), "nombre", null, 0, 1, Paciente.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPaciente_Apellido(), ecorePackage.getEString(), "apellido", null, 0, 1, Paciente.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPaciente_Edad(), ecorePackage.getEInt(), "edad", null, 0, 1, Paciente.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPaciente_Telefono(), ecorePackage.getEString(), "telefono", null, 0, 1, Paciente.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getPaciente__AgendarCita(), null, "agendarCita", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getPaciente__RealizarPago(), null, "realizarPago", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //OneClassAppPackageImpl
