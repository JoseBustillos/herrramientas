/**
 */
package com.example.demo.oneClassApp.impl;

import com.example.demo.oneClassApp.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OneClassAppFactoryImpl extends EFactoryImpl implements OneClassAppFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OneClassAppFactory init() {
		try {
			OneClassAppFactory theOneClassAppFactory = (OneClassAppFactory) EPackage.Registry.INSTANCE
					.getEFactory(OneClassAppPackage.eNS_URI);
			if (theOneClassAppFactory != null) {
				return theOneClassAppFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OneClassAppFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OneClassAppFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case OneClassAppPackage.PACIENTE:
			return createPaciente();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Paciente createPaciente() {
		PacienteImpl paciente = new PacienteImpl();
		return paciente;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OneClassAppPackage getOneClassAppPackage() {
		return (OneClassAppPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OneClassAppPackage getPackage() {
		return OneClassAppPackage.eINSTANCE;
	}

} //OneClassAppFactoryImpl
