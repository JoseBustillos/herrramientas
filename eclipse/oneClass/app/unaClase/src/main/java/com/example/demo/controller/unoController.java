package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.demo.oneClassApp.impl.PacienteImpl;
import com.example.demo.repository.oneRepository;

@RestController
// defecto -> localhost:8080
// localhost:8080/api/v1
@RequestMapping("/api/v1")
public class unoController {
	@Autowired
    private oneRepository oneRepository;
	
	// localhost:8080/api/v1/pacientes
	@GetMapping("/pacientes")
    public List<PacienteImpl> getAllPacientes(){
        return oneRepository.findAll();
    }

	// localhost:8080/api/v1/nuevoPaciente
    @RequestMapping(value = "/nuevoPaciente", method = RequestMethod.POST) 
    public PacienteImpl createMedicoImp(@ModelAttribute PacienteImpl paciente){
        return  oneRepository.save(paciente);
    }
}