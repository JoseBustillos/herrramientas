/**
 */
package com.example.demo.oneClassApp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Paciente</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.example.demo.oneClassApp.Paciente#getIdPaciente <em>Id Paciente</em>}</li>
 *   <li>{@link com.example.demo.oneClassApp.Paciente#getNombre <em>Nombre</em>}</li>
 *   <li>{@link com.example.demo.oneClassApp.Paciente#getApellido <em>Apellido</em>}</li>
 *   <li>{@link com.example.demo.oneClassApp.Paciente#getEdad <em>Edad</em>}</li>
 *   <li>{@link com.example.demo.oneClassApp.Paciente#getTelefono <em>Telefono</em>}</li>
 * </ul>
 *
 * @see com.example.demo.oneClassApp.OneClassAppPackage#getPaciente()
 * @model
 * @generated
 */
public interface Paciente extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Paciente</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Paciente</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Paciente</em>' attribute.
	 * @see #setIdPaciente(long)
	 * @see com.example.demo.oneClassApp.OneClassAppPackage#getPaciente_IdPaciente()
	 * @model
	 * @generated
	 */
	long getIdPaciente();

	/**
	 * Sets the value of the '{@link com.example.demo.oneClassApp.Paciente#getIdPaciente <em>Id Paciente</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Paciente</em>' attribute.
	 * @see #getIdPaciente()
	 * @generated
	 */
	void setIdPaciente(long value);

	/**
	 * Returns the value of the '<em><b>Nombre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nombre</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nombre</em>' attribute.
	 * @see #setNombre(String)
	 * @see com.example.demo.oneClassApp.OneClassAppPackage#getPaciente_Nombre()
	 * @model
	 * @generated
	 */
	String getNombre();

	/**
	 * Sets the value of the '{@link com.example.demo.oneClassApp.Paciente#getNombre <em>Nombre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nombre</em>' attribute.
	 * @see #getNombre()
	 * @generated
	 */
	void setNombre(String value);

	/**
	 * Returns the value of the '<em><b>Apellido</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Apellido</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Apellido</em>' attribute.
	 * @see #setApellido(String)
	 * @see com.example.demo.oneClassApp.OneClassAppPackage#getPaciente_Apellido()
	 * @model
	 * @generated
	 */
	String getApellido();

	/**
	 * Sets the value of the '{@link com.example.demo.oneClassApp.Paciente#getApellido <em>Apellido</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Apellido</em>' attribute.
	 * @see #getApellido()
	 * @generated
	 */
	void setApellido(String value);

	/**
	 * Returns the value of the '<em><b>Edad</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edad</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edad</em>' attribute.
	 * @see #setEdad(int)
	 * @see com.example.demo.oneClassApp.OneClassAppPackage#getPaciente_Edad()
	 * @model
	 * @generated
	 */
	int getEdad();

	/**
	 * Sets the value of the '{@link com.example.demo.oneClassApp.Paciente#getEdad <em>Edad</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edad</em>' attribute.
	 * @see #getEdad()
	 * @generated
	 */
	void setEdad(int value);

	/**
	 * Returns the value of the '<em><b>Telefono</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Telefono</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Telefono</em>' attribute.
	 * @see #setTelefono(String)
	 * @see com.example.demo.oneClassApp.OneClassAppPackage#getPaciente_Telefono()
	 * @model
	 * @generated
	 */
	String getTelefono();

	/**
	 * Sets the value of the '{@link com.example.demo.oneClassApp.Paciente#getTelefono <em>Telefono</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Telefono</em>' attribute.
	 * @see #getTelefono()
	 * @generated
	 */
	void setTelefono(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void agendarCita();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void realizarPago();

} // Paciente
