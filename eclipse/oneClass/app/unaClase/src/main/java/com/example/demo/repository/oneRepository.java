package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.oneClassApp.impl.PacienteImpl;

@Repository
public interface oneRepository extends JpaRepository<PacienteImpl, Long>{

}
